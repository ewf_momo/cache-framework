cache-framework
===============

OSChina 缓存框架重构

采用二级缓存结构

L1： 进程内缓存(ehcache) 
L2： 集中式缓存，支持多种集中式缓存服务器，如 Redis、Memcached 等

由于大量的缓存读取会导致 L2 的网络成为整个系统的瓶颈，因此 L1 的目标是降低对 L2 的读取次数

网络拓扑结构

         A1(L1_1)
Web  -              - L2 - DB
         A2(L1_2)

*** 数据读取 ***

1. 读取顺序  -> L1 -> L2 -> DB

2. 数据更新

A1 从数据库中读取最新数据，依次更新 L1_1 -> L2 ，广播清除某个缓存信息
A2 接收到广播，从 L1_2 中清除指定的缓存信息